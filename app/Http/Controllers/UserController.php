<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table user
        $user = User::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data User',
            'data'    => $user  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find user by ID
        $user = User::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data User',
            'data'    => $user 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $user = User::create([
            'name'     => $request->name,
            'email'   => $request->email,
            'password'   => md5($request->password),
            'role'   => $request->role
        ]);

        //success save to database
        if($user) {

            return response()->json([
                'success' => true,
                'message' => 'User Created',
                'data'    => $user  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'User Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $user
     * @return void
     */
    public function update(Request $request, User $user)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email' => 'required',
            'role' => 'required'
			
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find user by ID
        $user = User::findOrFail($user->id);

        if($user) {
			
			if (isset($request->password)) {
				$user->update([
                'name'     => $request->name,
				'email'   => $request->email,
				'password'   => md5($request->password),
				'role'   => $request->role
            ]);
			} else {
				$user->update([
                'name'     => $request->name,
				'email'   => $request->email,
				'role'   => $request->role
            ]);
			}
            

            return response()->json([
                'success' => true,
                'message' => 'User Updated',
                'data'    => $user  
            ], 200);

        }

        //data user not found
        return response()->json([
            'success' => false,
            'message' => 'user Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find user by ID
        $user = User::findOrfail($id);

        if($user) {

            //delete user
            $user->delete();

            return response()->json([
                'success' => true,
                'message' => 'User Deleted',
            ], 200);

        }

        //data user not found
        return response()->json([
            'success' => false,
            'message' => 'User Not Found',
        ], 404);
    }
}