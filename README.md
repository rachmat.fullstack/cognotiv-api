## How to install ?

Make sure in your notebook or PC already installed GIT, Composer, PHP and MySQL

1. Open your git bash and run "git clone git@gitlab.com:rachmat.fullstack/cognotiv-api.git"
2. Run "cd cognotiv-api"
3. Run "composer install", please wait until done.
4. Create new database in your MySQL and import file "cognotiv-db.sql"
5. Setup database connection in .env file
6. Run "php artisan serve"

You can try all API with postman, Import file "Cognotiv-API.postman_collection.json" in your postman.




